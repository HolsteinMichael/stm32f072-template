/*
 * @author  Daniel Schnell <dschnell@posteo.de>
 */

#include "spi_host_com.h"
#include "stm32f0xx_rcc.h"
#include "stm32f0xx_gpio.h"
#include "spi.h"
#include "FreeRTOS.h"
#include "logger.h"


void spi_host_com_activate_request_port()
{
    GPIO_InitTypeDef GPIO_InitStructure;

    /* Configure request to host port as output */
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
    GPIO_Init(GPIOD, &GPIO_InitStructure);
}

void spi_host_com_set_imx_request_active()
{
    GPIO_WriteBit(GPIOD, GPIO_Pin_2, Bit_SET);
}

void spi_host_com_set_imx_request_passive()
{
    GPIO_WriteBit(GPIOD, GPIO_Pin_2, Bit_RESET);
}

/* EOF */
